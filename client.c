#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <signal.h>
#include <math.h>

#define LOCALHOST "127.0.0.1"
#define STDIN 0
#define STDOUT 1
#define BUF_SIZE 1025
#define BROADCAST_IP "127.255.255.255"
#define PRJ_COUNT 7
#define GROUP_SIZE 5
#define DURATION 10

void swap(char *a, char* b) {
    char temp = *a;
    *a = *b;
    *b = temp;
}
void reverse_array(char* arr, int len) {
    int begin = 0, end = len - 1;

    while (begin < end) {
        swap((arr+begin), (arr+end));
        begin++;
        end--;
    }
}
int itoa(long val, char* str) {
    int i = 0, rem;

    if(val == 0) {
        str[i++] = '0';
        str[i] = '\0';
        return i;
    }

    while(val != 0) {
        rem = val % 10;
        str[i++] = rem + '0';
        val = val / 10;
    }

    str[i] = '\0';
    reverse_array(str, i);

    return i;
}
int startsWith(char* str, char* subStr, int subStrLen) {
    for(int i=0; i<subStrLen; i++) {
        if(str[i] != subStr[i]) {
            return 0;
        }
    }
    return 1;
}
void assignStr(char* lVal, char* str, int strLen) {
    for(int i=0; i<strLen; i++) {
        lVal[i] = str[i];
    }
    lVal[strLen] = '\0';
}

///////////////////////////////////////////////////////////////////////////////////

int makeAndConnectClient(int port) {
    int tmp = 1;
    struct sockaddr_in address;
    int sockFD = socket(AF_INET, SOCK_STREAM, 0);

    if (sockFD <= 0) {
        write(STDOUT, "- Socket creation failed!\n", 26 * sizeof(char));
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sockFD, SOL_SOCKET, SO_REUSEADDR,
            (void*) &tmp, sizeof(tmp)) < 0) {
        write(STDOUT, "- Setting socket options failed!\n", 33*sizeof(char));
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(LOCALHOST);
    address.sin_port = htons(port);

    if (connect(sockFD, (struct sockaddr *)&address, (socklen_t)sizeof(address)) < 0) {
        write(STDOUT, "- Connection failed!\n", 21 * sizeof(char));
        exit(EXIT_FAILURE);
    }

    write(STDOUT, "- Connected to server\n", 22 * sizeof(char));
    return sockFD;
}
int* getIntoGroup(int clientSockFD) {
    char buf[BUF_SIZE];
    int messLen;
    int *id_port_prjNum = (int *)malloc(3 * sizeof(int));

    while(1) {  // trying to get in to a group...
        write(STDOUT, "Type the project you desire : ", 30 * sizeof(char));
        read(STDIN, buf, BUF_SIZE);
        id_port_prjNum[2] = atoi(buf+1);

        if(send(clientSockFD, buf, BUF_SIZE, 0) < 0) {  // send answer
            write(STDOUT, "- Sending failed!\n", 18 * sizeof(char));
            exit(EXIT_FAILURE);
        }  
        messLen = recv(clientSockFD, buf, BUF_SIZE, 0); // get answer for joining

        if(startsWith(buf, "added_", 6)) {
            write(STDOUT, "- Succesfully added with id = ", 30 * sizeof(char));
            write(STDOUT, buf+6, 1 * sizeof(char));
            write(STDOUT, "\n", 1 * sizeof(char));
            id_port_prjNum[0] = atoi(buf+6);

            messLen = recv(clientSockFD, buf, BUF_SIZE, 0); //get port
            if (startsWith(buf, "BP_", 3)) {
                id_port_prjNum[1] = atoi(buf + 3);
                
                char holder[BUF_SIZE];
                int len = itoa(id_port_prjNum[1], holder);
                write(STDOUT, "- Broadcast port = ", 19 * sizeof(char));
                write(STDOUT, holder, len * sizeof(char));
                write(STDOUT, "\n", 1 * sizeof(char));
                break;
            }
        }
        else if(startsWith(buf, "terminated", 10)) {
            id_port_prjNum[0] = -1;
            return id_port_prjNum;
        }
        else {
            write(STDOUT, buf, messLen * sizeof(char));
        }
    }
    return id_port_prjNum;
}
void getGreetings(int clientSockFD) {
    char buf[BUF_SIZE];
    int messLen;

    if(send(clientSockFD, "greetings", 9, 0) < 0) {
        write(STDOUT, "- Sending failed!\n", 18 * sizeof(char));
        exit(EXIT_FAILURE);
    }
    messLen = recv(clientSockFD, buf, BUF_SIZE, 0);
    write(STDOUT, buf, messLen * sizeof(char));
}
int createUDPSocket(int port) {
    int tmp1 = 1, tmp2 = 1, tmp3 = 1;

    int clientSockFD_UDP = socket(AF_INET, SOCK_DGRAM, 0);
    if (clientSockFD_UDP < 0) {
        write(STDOUT, "- Socket creation failed!\n", 26*sizeof(char));
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(BROADCAST_IP);
    address.sin_port = htons(port);

    if (setsockopt(clientSockFD_UDP, SOL_SOCKET, SO_BROADCAST, 
            (void*) &tmp1, sizeof(tmp1)) < 0) {
        write(STDOUT, "- Setting socket options failed!\n", 33*sizeof(char));
        exit(EXIT_FAILURE);
    }
    if (setsockopt(clientSockFD_UDP, SOL_SOCKET, SO_REUSEADDR,
            (void*) &tmp2, sizeof(tmp2)) < 0) {
        write(STDOUT, "- Setting socket options failed!\n", 33*sizeof(char));
        exit(EXIT_FAILURE);
    }
    if (setsockopt(clientSockFD_UDP, SOL_SOCKET, SO_REUSEPORT,
            (void*) &tmp3, sizeof(tmp3)) < 0) {
        write(STDOUT, "- Setting socket options failed!\n", 33*sizeof(char));
        exit(EXIT_FAILURE);
    }

    if(bind(clientSockFD_UDP, (struct sockaddr*) &address, (socklen_t) sizeof(address)) < 0) {
        write(STDOUT, "- Socket binding failed!\n", 25*sizeof(char));
        exit(EXIT_FAILURE);
    }

    return clientSockFD_UDP;
}
void sigHandler(int sigNum) {
    return;
}
void sendPrice(int clientSockFD_UDP, int id, struct sockaddr_in address) {
    char buf[BUF_SIZE];
    char holder[BUF_SIZE];
    int len;
    int r;
    assignStr(buf, "price_", 6);
    len = itoa(id, holder);
    assignStr(buf + 6, holder, len);
    assignStr(buf + 6 + len, "_", 1);

    write(STDOUT, "It's your turn! Type your price: ", 33 * sizeof(char));
    
    signal(SIGALRM, sigHandler);
    siginterrupt(SIGALRM, 1);
    alarm(DURATION);
     if(r = read(STDIN, buf + 7 + len, BUF_SIZE) < 0) {
        write(STDOUT, "- Time is up!\n", 14 * sizeof(char));
        assignStr(buf + 7 + len, "TO", 2);
     }
    alarm(0);

    if(sendto(clientSockFD_UDP, buf, BUF_SIZE, 0, (struct sockaddr *)&address, sizeof(address)) < 0) {
        write(STDOUT, "- Sending price failed!\n", 24 * sizeof(char));
        exit(EXIT_FAILURE);
    }
}
void sendNewTurn(int turn, int clientSockFD_UDP, struct sockaddr_in address) {
    char buf[BUF_SIZE];
    char holder[BUF_SIZE];
    int len;
    assignStr(buf, "turn_", 5);
    len = itoa(turn + 1, holder);
    assignStr(buf + 5, holder, len);

    if(sendto(clientSockFD_UDP, buf, BUF_SIZE, 0, (struct sockaddr *)&address, sizeof(address)) < 0) {
        write(STDOUT, "- Sending turn failed!\n", 23 * sizeof(char));
        exit(EXIT_FAILURE);
    }
}
int* doAuction(int clientSockFD_UDP, int id, int port) {
    char buf[BUF_SIZE];
    int turn = 1;
    int *prices = (int *)malloc(GROUP_SIZE * sizeof(int));

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(BROADCAST_IP);
    address.sin_port = htons(port);

    while(1) {
        if(turn == GROUP_SIZE + 1)
            break;

        if(turn == id)
            sendPrice(clientSockFD_UDP, id, address);

        recvfrom(clientSockFD_UDP, buf, BUF_SIZE, 0, NULL, 0);  // get new price
        if(startsWith(buf, "price_", 6)) {
            if(startsWith(buf+8, "TO", 2))
                prices[(buf[6] - '0') - 1] = INFINITY;
            else
                prices[(buf[6]-'0')-1] = atoi(buf+8);
        }

        if(turn == id)
            sendNewTurn(turn, clientSockFD_UDP, address);

        recvfrom(clientSockFD_UDP, buf, BUF_SIZE, 0, NULL, 0);  // get new turn
        if(startsWith(buf, "turn_", 5)) {
            turn = atoi(buf+5);
        }
    }
    return prices;
}
int giveWinnerID(int* prices, int pricesLen) {
    int minPrice = prices[0];
    int minIndex = 0;
    int counter = 0;

    for(int i=0; i<pricesLen; i++) {
        if(prices[i] < minPrice) {
            minPrice = prices[i];
            minIndex = i;
        }
    }

    for(int i=0; i<pricesLen; i++) {
        if(prices[i] == minPrice) {
            counter++;
        }
    }

    if (counter > 1)
        return -1;
    else
        return minIndex + 1;
}
void announceWinner(int myID, int winnerID, int prjNum, int clientSockFD) {
    char buf[BUF_SIZE];

    if(winnerID > 0) {  // has a winner...
        assignStr(buf, "won_", 4);
        itoa(prjNum, buf+4);

        if(myID == winnerID) {
            write(STDOUT, "- You won!\n", 11 * sizeof(char));
            if(send(clientSockFD, buf, BUF_SIZE, 0) < 0) {
                write(STDOUT, "- Sending winner failed!\n", 25 * sizeof(char));
                exit(EXIT_FAILURE);
            }
        }
    }
    else {  // there is no winner...
        assignStr(buf, "tie_", 4);
        itoa(prjNum, buf+4);

        if(myID == GROUP_SIZE) {
            if(send(clientSockFD, buf, BUF_SIZE, 0) < 0) {
                write(STDOUT, "- Sending winner failed!\n", 25 * sizeof(char));
                exit(EXIT_FAILURE);
            }
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        write(STDOUT, "- Not enough arguments!\n", 24 * sizeof(char));
        return 0;
    }

    int clientSockFD = makeAndConnectClient(atoi(argv[1]));
    while(1) {
        getGreetings(clientSockFD);
        int* id_port_prjNum = getIntoGroup(clientSockFD);
        if(id_port_prjNum[0] == -1)
            break;

        // got in to a group...UDP part...
        int clientSockFD_UDP = createUDPSocket(id_port_prjNum[1]);
        int* prices = doAuction(clientSockFD_UDP, id_port_prjNum[0], id_port_prjNum[1]);
        int winnerID = giveWinnerID(prices, GROUP_SIZE);
        announceWinner(id_port_prjNum[0], winnerID, id_port_prjNum[2], clientSockFD);

        close(clientSockFD_UDP);
    }
    return 0;
}