#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/select.h>

#define LOCALHOST "127.0.0.1"
#define STDIN 0
#define STDOUT 1
#define EMPTY -1
#define BUF_SIZE 1025
#define GROUPS_BASE_PORT 5000
#define PRJ_COUNT 7
#define GROUP_SIZE 5


struct Prj {
    int members[5];
    int memberCount;
    int winner;
    int port;
    int closed;
};

int startServer(int port, int qSize);
int acceptClient(int serverSockFD);
void greetClient(int clientSockFD, struct Prj *prjs, int prjCount);
struct Prj *initProjects(int count);
void addToPrjGroup(struct Prj *prjs, int clientFD, int prjIndex);
int isGroupFull(struct Prj *prjs, int prjIndex);
void sendPortToMembers(struct Prj *prjs, int prjIndex, int port);
void handleConnections(int clientSockFD, int *clients, struct Prj *projects, char *buf);

///////////////////////////////////////////////////////////////////////////////////

void swap(char *a, char* b) {
    char temp = *a;
    *a = *b;
    *b = temp;
}
void reverse_array(char* arr, int len) {
    int begin = 0, end = len - 1;

    while (begin < end) {
        swap((arr+begin), (arr+end));
        begin++;
        end--;
    }
}
int itoa(long val, char* str) {
    int i = 0, rem;

    if(val == 0) {
        str[i++] = '0';
        str[i] = '\0';
        return i;
    }

    while(val != 0) {
        rem = val % 10;
        str[i++] = rem + '0';
        val = val / 10;
    }

    str[i] = '\0';
    reverse_array(str, i);

    return i;
}
void assignStr(char* lVal, char* str, int strLen) {
    for(int i=0; i<strLen; i++) {
        lVal[i] = str[i];
    }
    lVal[strLen] = '\0';
}
int startsWith(char* str, char* subStr, int subStrLen) {
    for(int i=0; i<subStrLen; i++) {
        if(str[i] != subStr[i]) {
            return 0;
        }
    }
    return 1;
}

///////////////////////////////////////////////////////////////////////////////////

int* initClientsArr(int len) {
    int *arr = (int *)malloc(len * sizeof(int));
    for(int i=0; i<len; i++) {
        arr[i] = EMPTY;
    }
    return arr;
}
void addToClientsArr(int* arr, int len, int new) {
    for(int i=0; i<len; i++) {
        if(arr[i] == EMPTY) {
            arr[i] = new;
            break;
        }
    }
}
void removeFromClientsArr(int* arr, int len, int r) {
    for(int i=0; i<len; i++) {
        if(arr[i] == r) {
            arr[i] = EMPTY;
            break;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////

int startServer(int port, int qSize) {
    int tmp = 1;
    int sockFD = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in address;

    if (sockFD <= 0) {
        write(STDOUT, "- Socket creation failed!\n", 26 * sizeof(char));
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sockFD, SOL_SOCKET, SO_REUSEADDR,
            (void*) &tmp, sizeof(tmp)) < 0) {
        write(STDOUT, "- Setting socket options failed!\n", 33*sizeof(char));
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(LOCALHOST);
    address.sin_port = htons(port);

    if (bind(sockFD, (struct sockaddr *)&address, (socklen_t)sizeof(address)) < 0) {
        write(STDOUT, "- Socket binding failed!\n", 25 * sizeof(char));
        exit(EXIT_FAILURE);
    }

    if (listen(sockFD, qSize) < 0) {
        write(STDOUT, "- Listening failed!\n", 20 * sizeof(char));
        exit(EXIT_FAILURE);
    }

    write(STDOUT, "- Server running\n", 17 * sizeof(char));
    return sockFD;
}
int acceptClient(int serverSockFD) {
    char buf[BUF_SIZE];
    char holder[BUF_SIZE];
    int messLen;

    struct sockaddr_in clientAddress;
    int len = sizeof(clientAddress);

    int clientSockFD = accept(serverSockFD, (struct sockaddr *)&clientAddress, (socklen_t*)&len);

    if (clientSockFD < 0) {
        write(STDOUT, "- Client acceptance failed!\n", 28 * sizeof(char));
        exit(EXIT_FAILURE);
    }

    assignStr(buf, "- Client with FD = ", 19);
    messLen = itoa(clientSockFD, buf+19);
    assignStr(buf + 19 + messLen, " connected!", 11);
    write(STDOUT, buf, (19 + messLen + 11) * sizeof(char));
    write(STDOUT, "\n", 1 * sizeof(char));

    return clientSockFD;
}
void greetClient(int clientSockFD, struct Prj *prjs, int prjCount) {
    char tempBuf[100];
    int len = 0;

    assignStr(tempBuf, "\nHi!\nAvailable projects are: ", 29);
    len += 29;
    for(int i=0; i<prjCount; i++) {
        if((prjs[i].memberCount < GROUP_SIZE) && (prjs[i].winner == 0)) {
            tempBuf[len++] = 'p';
            len += itoa(i+1, tempBuf+len);
            tempBuf[len++] = ' ';
        }
    }
    tempBuf[len++] = '\n';

    if(send(clientSockFD, tempBuf, len * sizeof(char), 0) < 0) {
        write(STDOUT, "- Greeting failed!\n", 19 * sizeof(char));
        exit(EXIT_FAILURE);
    }
    write(STDOUT, "- Greeting sent to ", 19 * sizeof(char));
    len = itoa(clientSockFD, tempBuf);
    write(STDOUT, tempBuf, len * sizeof(char));
    write(STDOUT, "\n", 1 * sizeof(char));
}
struct Prj* initProjects(int count) {
    struct Prj *prjs = (struct Prj *)malloc(count * sizeof(struct Prj));

    for(int i=0; i<count; i++) {
        prjs[i].winner = 0;
        prjs[i].port = 0;
        prjs[i].memberCount = 0;
        prjs[i].closed = 0;
    }
    return prjs;
}
void addToPrjGroup(struct Prj *prjs, int clientFD, int prjIndex) {
    char holder[20];
    char holder2[20];
    prjs[prjIndex].members[prjs[prjIndex].memberCount] = clientFD;
    prjs[prjIndex].memberCount++;

    write(STDOUT, "- Client with FD = ", 19 * sizeof(char));
    int len = itoa(clientFD, holder);
    write(STDOUT, holder , len * sizeof(char));
    write(STDOUT, " added to group ", 16 * sizeof(char));
    len = itoa(prjIndex+1, holder);
    write(STDOUT, holder, len * sizeof(char));
    write(STDOUT, "\n", 1 * sizeof(char));


    assignStr(holder, "added_",6);
    len = itoa(prjs[prjIndex].memberCount, holder2);
    assignStr(holder+6, holder2, len);

    if (send(clientFD, holder, (len+6) * sizeof(char), 0) < 0) {
        write(STDOUT, "- Sending failed!\n", 18 * sizeof(char));
        exit(EXIT_FAILURE);
    }
}
int isGroupFull(struct Prj *prjs, int prjIndex) {
    if(prjs[prjIndex].memberCount == GROUP_SIZE) {
        return GROUPS_BASE_PORT + prjIndex;
    }
    return 0;
}
void sendPortToMembers(struct Prj *prjs, int prjIndex, int port) {
    char holder[10];
    int len;
    prjs[prjIndex].port = port;
    assignStr(holder, "BP_", 3);
    len = itoa(port, holder+3);

    write(STDOUT, "- Broadcast port sent to: ", 26 * sizeof(char));
    for(int i=0; i<GROUP_SIZE; i++) {
        char holder2[10];
        int len2 = itoa(prjs[prjIndex].members[i], holder2);
        write(STDOUT, holder2, len2 * sizeof(char));
        write(STDOUT, "  ", 2 * sizeof(char));

        if(send(prjs[prjIndex].members[i], holder, len+3, 0) < 0) {
            write(STDOUT, "- Sending broadcast port failed!\n", 33 * sizeof(char));
            exit(EXIT_FAILURE);
        }
    }
    write(STDOUT, "\n", 1 * sizeof(char));
}

void handleConnections(int clientSockFD, int* clients, struct Prj* projects, char* buf) {
    if (startsWith(buf, "terminate", 9)) {
        char holder[BUF_SIZE];
        int len;

        if (send(clientSockFD, "terminated", 10, 0) < 0) {
            write(STDOUT, "- Sending failed!\n", 18 * sizeof(char));
            exit(EXIT_FAILURE);
        }
        close(clientSockFD);
        len = itoa(clientSockFD, holder);
        write(STDOUT, "- Terminated ", 13 * sizeof(char));
        write(STDOUT, holder, len * sizeof(char));
        write(STDOUT, " !\n", 3 * sizeof(char));

        removeFromClientsArr(clients, FD_SETSIZE, clientSockFD);
    }
    else if (startsWith(buf, "p", 1)) { // handle adding to project group
        if ((atoi(buf + 1) <= PRJ_COUNT) && (isGroupFull(projects, atoi(buf + 1) - 1) == 0)) {
            addToPrjGroup(projects, clientSockFD, atoi(buf + 1) - 1);
            int groupPort = isGroupFull(projects, atoi(buf + 1) - 1);
            if (groupPort) {
                sendPortToMembers(projects, atoi(buf + 1) - 1, groupPort);
            }
        }
        else { // no more room!
            if (send(clientSockFD, "- Can't access this project!\n", 29, 0) < 0) {
                write(STDOUT, "- Sending failed!\n", 18 * sizeof(char));
                exit(EXIT_FAILURE);
            }
        }
    }
    else if (startsWith(buf, "greetings", 9)) {
        greetClient(clientSockFD, projects, PRJ_COUNT);
    }
    else if (startsWith(buf, "won_", 4)) {
        char holder1[BUF_SIZE];
        int len1, len2;

        int prjNum = atoi(buf + 4);
        projects[prjNum - 1].closed = 1;
        projects[prjNum - 1].winner = clientSockFD;

        assignStr(buf, "- Project ", 10);
        len1 = itoa(prjNum, holder1);
        assignStr(buf + 10, holder1, len1);
        assignStr(buf + 10 + len1, " is won by client with FD = ", 28);
        len2 = itoa(clientSockFD, holder1);
        assignStr(buf + 10 + len1 + 28, holder1, len2);
        write(STDOUT, buf, (10 + len1 + 28 + len2) * sizeof(char));
        write(STDOUT, "\n", 1 * sizeof(char));
    }
    else if (startsWith(buf, "tie_", 4)) {
        char holder1[BUF_SIZE];
        int len1;
        int prjNum = atoi(buf + 4);
        // projects[prjNum - 1].closed = 1;
        // projects[prjNum - 1].winner = -1;
        projects[prjNum - 1].memberCount = 0;

        assignStr(buf, "- No winner for project ", 24);
        len1 = itoa(prjNum, holder1);
        assignStr(buf + 24, holder1, len1);
        write(STDOUT, buf, (24 + len1) * sizeof(char));
        write(STDOUT, "\n", 1 * sizeof(char));
    }
    else {
        write(STDOUT, "- Request not found!\n", 21 * sizeof(char));
    }
}
///////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
    if(argc != 2) {
        write(STDOUT, "- Not enough arguments!\n", 24 * sizeof(char));
        exit(EXIT_FAILURE);
    }
    char buf[BUF_SIZE];
    int clientSockFD;
    fd_set FDSet;

    int serverSockFD = startServer(atoi(argv[1]), 5);
    struct Prj *projects = initProjects(PRJ_COUNT);
    int* clients = initClientsArr(FD_SETSIZE);

    while (1) {
        FD_ZERO(&FDSet);
        FD_SET(serverSockFD, &FDSet);   // adding server

        for(int i=0; i<FD_SETSIZE; i++) {   // adding active clients
            if(clients[i] != EMPTY) {
                FD_SET(clients[i], &FDSet);
            }
        }

        if (select(FD_SETSIZE, &FDSet, NULL, NULL, NULL) < 0) {
            write(STDOUT, "- Selection failed!\n", 20 * sizeof(char));
            exit(EXIT_FAILURE);
        }

        for(int i=0; i<FD_SETSIZE; i++) {
            if(FD_ISSET(i, &FDSet)) {
                if(i == serverSockFD) { // checking for server activity
                    clientSockFD = acceptClient(serverSockFD);
                    addToClientsArr(clients, FD_SETSIZE, clientSockFD);
                }
                else {  // checking for clients activities
                    recv(i, buf, BUF_SIZE, 0);
                    handleConnections(i, clients, projects, buf);
                }
            }
        }
    }

    return 0;
}